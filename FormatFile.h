#pragma once

#include "Format.h"
#include "DataSave.h"

class FormatFile:public Format
{
	KIR4::gdi_bitmap
		bitmap;
	StructOfDataUpload
		sodu;
	std::list<std::wstring>
		files;
	std::list<std::wstring>::iterator
		it;
public:
	FormatFile(General *general, const std::list<std::wstring> &files):Format(general), files(files)
	{
		it = this->files.begin();
	}
	virtual void Load()
	{
		if (bitmap.load(it->c_str()))
		{
			unsigned long
				size = 0;
			KIR4::socket_message
				*format = NULL;

			KIR4::gdi_bitmap::buffer
				data;

			std::wstring
				extension;
			auto
				pos = it->find_last_of(L".");
			if (pos < it->length())
				extension = it->substr(pos + 1);

			if (extension == L"png")
			{
				clog << KIR4::PURPLE << "png" << KIR4::eol;
				data = bitmap.buffer_save(L"image/png");
				format = new KIR4::socket_message_dynamic(CodeFormatPNG);
			}
			else
			{
				clog << KIR4::PURPLE << "not png" << KIR4::eol;
				data = bitmap.buffer_save(L"image/jpeg");
				format = new KIR4::socket_message_dynamic(CodeFormatJPG);
			}

			size = data.size();
			format->set_int64(size);

			clog << KIR4::BLUE << "STARTER SIZE: " << KIR4::LBLUE << bitmap.width()*bitmap.height() * 4 << "\t" << double(bitmap.width()*bitmap.height() * 4) / (1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;
			clog << KIR4::BLUE << "REDUCED SIZE: " << KIR4::LBLUE << size << "\t" << size / double(1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;

			sodu.Destroy();
			sodu.data = (char*)data.release();
			sodu.size = size;
			sodu.format = format;
		}
		else
			clog << KIR4::LRED << "Load Unsuccess!" << KIR4::eol;
	}
	virtual StructOfDataUpload *GetNext()
	{
		if (it != files.end())
		{
			Load();
			it++;
			return &sodu;
		}
		else
			return NULL;
	}
	virtual void Save()
	{
		for (auto &it : files)
		{
			if (bitmap.load(it.c_str()))
				BitmapSave(general, bitmap);
		}
	}
};