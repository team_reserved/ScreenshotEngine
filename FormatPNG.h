#pragma once

#include "Format.h"
#include "DataSave.h"

class FormatPNG:public Format
{
	KIR4::gdi_bitmap
		bitmap;
	StructOfDataUpload
		sodu;
	bool
		IsGetting = false;
public:
	FormatPNG(General *general, KIR4::gdi_bitmap &bitmap):Format(general), bitmap(bitmap)
	{
		clog << bitmap.width() << " * " << bitmap.height() << KIR4::eol;
	}
	inline void Load()
	{
		if (bitmap)
		{
			unsigned long
				size = 0;
			KIR4::socket_message
				*format = NULL;

			auto
				data = bitmap.buffer_save();
			size = data.size();

			clog << KIR4::BLUE << "STARTER SIZE: " << KIR4::LBLUE << bitmap.width()*bitmap.height() * 4 << "\t" << double(bitmap.width()*bitmap.height() * 4) / (1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;
			clog << KIR4::BLUE << "REDUCED SIZE: " << KIR4::LBLUE << size << "\t" << size / double(1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;

			format = new KIR4::socket_message_dynamic(CodeFormatPNG);
			format->set_int64(size);

			sodu.Destroy();
			sodu.data = (char*)data.release();
			sodu.size = size;
			sodu.format = format;
		}
	}
	virtual StructOfDataUpload *GetNext()
	{
		if (IsGetting)
			return NULL;
		else
		{
			IsGetting = true;
			Load();
			return &sodu;
		}
	}
	inline void Save()
	{
		if (bitmap)
			BitmapSave(general, bitmap);
	}
};