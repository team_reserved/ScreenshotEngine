#pragma once

#include "Format.h"
#include "DataSave.h"

class FormatRGBA:public Format
{
	KIR4::gdi_bitmap
		bitmap;
	StructOfDataUpload
		sodu;
	bool
		IsGetting = false;
public:
	FormatRGBA(General *general, KIR4::gdi_bitmap &bitmap):Format(general), bitmap(bitmap)
	{
		clog << bitmap.width() << " * " << bitmap.height() << KIR4::eol;
	}
	inline void Load()
	{
		if (bitmap)
		{
			char
				*data = NULL;
			unsigned long
				size = 0;
			KIR4::socket_message
				*format = NULL;

			int
				width = bitmap.width(),
				height = bitmap.height();

			KIR4::gdi_bitmap::lock_BGRA
				lock_bgr;

			lock_bgr.lock(bitmap);

			size = width * height * lock_bgr.pixel_bytes();
			data = new char[size];

			for (int y = 0; y < height; ++y)
				CopyMemory(data + y*lock_bgr.row_pixel_bytes(), lock_bgr.pixels() + lock_bgr.pitch()*y, lock_bgr.row_pixel_bytes());

			lock_bgr.unlock();

			format = new KIR4::socket_message_dynamic(CodeFormatRGBA);
			format->set_int32(width);
			format->set_int32(height);

			clog << KIR4::BLUE << "STARTER SIZE: " << KIR4::LBLUE << bitmap.width()*bitmap.height() * 4 << "\t" << double(bitmap.width()*bitmap.height() * 4) / (1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;
			clog << KIR4::BLUE << "REDUCED SIZE: " << KIR4::LBLUE << size << "\t" << size / double(1024 * 1024) << KIR4::BLUE << " MB" << KIR4::eol;

			sodu.Destroy();
			sodu.data = data;
			sodu.size = size;
			sodu.format = format;
		}
	}
	virtual StructOfDataUpload *GetNext()
	{
		if (IsGetting)
			return NULL;
		else
		{
			IsGetting = true;
			Load();
			return &sodu;
		}
	}
	inline void Save()
	{
		if (bitmap)
			BitmapSave(general, bitmap);
	}
};