#pragma once

KIR4::bitmap Convert(KIR4::gdi_bitmap &gdi_bitmap)
{
	KIR4::bitmap
		bitmap = al_create_bitmap(gdi_bitmap.width(), gdi_bitmap.height());

	KIR4::gdi_bitmap::lock_BGRA
		gdi_lock_rgba;
	KIR4::bitmap::lock_BGRA
		lock_rgba;

	gdi_lock_rgba.lock(gdi_bitmap);
	lock_rgba.lock(bitmap);

	unsigned __int8
		*src,
		*dst;
	for (int y = 0; y < gdi_bitmap.height(); ++y)
	{
		src = gdi_lock_rgba.pixels() + gdi_lock_rgba.pitch()*y;
		dst = lock_rgba.pixels() + lock_rgba.pitch()*y;
		CopyMemory(dst, src, gdi_lock_rgba.row_pixel_bytes());
	}

	gdi_lock_rgba.unlock();
	lock_rgba.unlock();

	return bitmap;
}

KIR4::gdi_bitmap Convert(KIR4::bitmap &bitmap)
{
	KIR4::gdi_bitmap
		gdi_bitmap = new Gdiplus::Bitmap(bitmap.width(), bitmap.height());

	KIR4::gdi_bitmap::lock_BGRA
		gdi_lock_rgba;
	KIR4::bitmap::lock_BGRA
		lock_rgba;

	gdi_lock_rgba.lock(gdi_bitmap);
	lock_rgba.lock(bitmap);

	unsigned __int8
		*src,
		*dst;
	for (int y = 0; y < gdi_bitmap.height(); ++y)
	{
		dst = gdi_lock_rgba.pixels() + gdi_lock_rgba.pitch()*y;
		src = lock_rgba.pixels() + lock_rgba.pitch()*y;
		CopyMemory(dst, src, lock_rgba.row_pixel_bytes());
	}

	gdi_lock_rgba.unlock();
	lock_rgba.unlock();

	return gdi_bitmap;
}