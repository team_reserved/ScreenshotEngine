#pragma once

KIR4::bitmap GetRandomBitmap(int max_width, int max_height)
{
	KIR4::bitmap
		bitmap(al_create_bitmap(rand() % max_width + 1, rand() % max_height + 1));
	int
		height = bitmap.height(),
		width = bitmap.width();

	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE);
	ALLEGRO_LOCKED_REGION
		*lr = al_lock_bitmap(bitmap, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);
	for (int y = 0; y < height; y++)
	{
		uint8_t
			*ptr = (uint8_t *)lr->data + y*lr->pitch;

		for (int x = 0; x < width; x++)
		{
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;

		}
	}
	al_unlock_bitmap(bitmap);
	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY);

	int
		rectangle_count = rand() % 24 + 10;

	bitmap.set_target();

	for (int i = 0; i < rectangle_count; i++)
		al_draw_filled_rectangle(rand() % width, rand() % height, rand() % width, rand() % height, al_map_rgb(rand() % 256, rand() % 256, rand() % 256));

	return bitmap;
}
KIR4::bitmap GetFixSizeBitmap(int width, int height)
{
	KIR4::bitmap
		bitmap(al_create_bitmap(width, height));

	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE);
	ALLEGRO_LOCKED_REGION
		*lr = al_lock_bitmap(bitmap, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);
	for (int y = 0; y < height; y++)
	{
		uint8_t
			*ptr = (uint8_t *)lr->data + y*lr->pitch;

		for (int x = 0; x < width; x++)
		{
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;
			*ptr = rand() % 256;
			*ptr++;

		}
	}
	al_unlock_bitmap(bitmap);
	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY);

	return bitmap;
}