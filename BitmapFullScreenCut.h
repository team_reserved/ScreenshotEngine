#pragma once

#include "GetFullScreen.h"

class FullScreenCutEvent:public KIR4::display_event_handle
{
	General
		&general;
	KIR4::bitmap
		&background,
		cutted;
	bool
		mouse_lock = false;
	int
		mouse_lock_x,
		mouse_lock_y;
public:
	FullScreenCutEvent(General &general, int x, int y, KIR4::bitmap &background):
		general(general),
		KIR4::display_event_handle(KIR4_25_CPS, "UYI BitmapFullScreenCut", general.GetDisplayWidth(), general.GetDisplayHeight(), ALLEGRO_RESIZABLE),
		background(background)
	{
		set_position(x, y);
	}

	virtual inline void EVENT_MOUSE_AXES(int X, int Y)
	{
		redrawn();
	}
	virtual inline void EVENT_DRAW(int X, int Y)
	{
		background.draw_scaled(0, 0, width(), height());
		if (mouse_lock)
		{
			al_draw_rectangle(mouse_lock_x, mouse_lock_y, mousex(), mousey(), al_map_rgba(0, 0, 0, 100), 2);
			al_draw_filled_rectangle(mouse_lock_x, mouse_lock_y, mousex(), mousey(), al_map_rgba(100, 100, 100, 100));
		}
	}
	virtual inline void EVENT_TIMER(double cps)
	{
	}
	virtual inline void EVENT_TICK()
	{
	}
	virtual inline void EVENT_MOUSE_BUTTON_DOWN(int BUTTON, int X, int Y)
	{
		if (BUTTON == KIR4::MOUSE_BUTTON_LEFT)
		{
			mouse_lock = true;
			mouse_lock_x = X;
			mouse_lock_y = Y;
		}
		else if (BUTTON == KIR4::MOUSE_BUTTON_RIGHT)
		{
			mouse_lock = false;
			redrawn();
		}
	}
	virtual inline void EVENT_MOUSE_BUTTON_UP(int BUTTON, int X, int Y)
	{
		if (BUTTON == KIR4::MOUSE_BUTTON_LEFT && mouse_lock)
		{
			if (X >= width())
				X = width() - 1;
			else if (X < 0)
				X = 0;
			if (Y >= height())
				Y = height() - 1;
			else if (Y < 0)
				Y = 0;

			int
				max_x,
				max_y,
				min_y,
				min_x;

			if (mouse_lock_x > X)
			{
				max_x = mouse_lock_x;
				min_x = X;
			}
			else
			{
				max_x = X;
				min_x = mouse_lock_x;
			}

			if (mouse_lock_y > Y)
			{
				max_y = mouse_lock_y;
				min_y = Y;
			}
			else
			{
				max_y = Y;
				min_y = mouse_lock_y;
			}

			cutted = background.clone(min_x / double(width())*background.width(), min_y / double(height())*background.height(), (max_x - min_x) / double(width())*background.width() + 1, (max_y - min_y) / double(height()) * background.height() + 1);
			kill();
		}
	}
	virtual inline bool EVENT_KEY_DOWN(int KEY)
	{
		return false;
	}
	virtual inline bool EVENT_KEY_UP(int KEY)
	{
		return false;
	}
	virtual inline void EVENT_KEY_CHAR(int KEY)
	{
	}
	virtual inline void RESIZED(int WIDTH, int HEIGHT)
	{
		redrawn();
	}
	virtual inline void EVENT_OTHER(int EVENT)
	{
	}

	inline KIR4::bitmap GetCutted()
	{
		return cutted;
	}
};


KIR4::bitmap GetFullScreenCutBitmap(General &general)
{
	KIR4::bitmap
		bitmap;
	int
		x, y,
		min_x = 0, min_y = 0, max_x = 0, max_y = 0;

	POINT
		p;
	GetCursorPos(&p);
	ALLEGRO_MONITOR_INFO
		monitor_info;

	for (int i = al_get_num_video_adapters(); i > 0; i--)
	{
		al_get_monitor_info(i - 1, &monitor_info);

		if (min_x > monitor_info.x1)
			min_x = monitor_info.x1;
		if (max_x < monitor_info.x2)
			max_x = monitor_info.x2;

		if (min_y > monitor_info.y1)
			min_y = monitor_info.y1;
		if (max_y < monitor_info.y2)
			max_y = monitor_info.y2;

		if (monitor_info.x1 <= p.x && monitor_info.y1 <= p.y && monitor_info.x2 > p.x && monitor_info.y2 > p.y)
		{
			x = monitor_info.x1;
			y = monitor_info.y1;
		}
	}
	bitmap = al_create_bitmap(max_x - min_x + 1, max_y - min_y + 1);
	bitmap.set_target();
	al_clear_to_color(al_map_rgb(20, 20, 20));

	for (int i = al_get_num_video_adapters(); i > 0; i--)
	{
		al_get_monitor_info(i - 1, &monitor_info);

		GetScreen(monitor_info.x1, monitor_info.y1, monitor_info.x2, monitor_info.y2).draw(monitor_info.x1, monitor_info.y1);
	}

	if (general.GetDisplayWidth() > 0 && general.GetDisplayHeight() > 0)
	{
		FullScreenCutEvent
			event(general, x, y, bitmap);

		event.set_target();

		event.run();

		return event.GetCutted();
	}

	return KIR4::bitmap();
}