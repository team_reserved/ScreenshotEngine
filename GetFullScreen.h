#pragma once

#include <Windows.h>

KIR4::gdi_bitmap GetScreen(int x1, int y1, int x2, int y2)
{
	HDC
		dc = GetWindowDC(GetDesktopWindow());

	HDC
		hmemdc = CreateCompatibleDC(dc);

	HBITMAP
		hbmp = CreateCompatibleBitmap(dc, x2 - x1, y2 - y1);

	HBITMAP
		holdbmp = (HBITMAP)SelectObject(hmemdc, hbmp);

	StretchBlt(hmemdc, 0, 0, x2 - x1, y2 - y1, dc, x1, y1, x2 - x1, y2 - y1, SRCCOPY);
	SelectObject(hmemdc, holdbmp);


	return new Gdiplus::Bitmap(hbmp, NULL);
}