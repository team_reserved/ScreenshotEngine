#pragma once

#include "GetFullScreen.h"

class MonitorCutEvent:public KIR4::display_event_handle
{
	General
		&general;
	KIR4::bitmap
		&background,
		cutted;
	bool
		mouse_lock = false;
	int
		mouse_lock_x,
		mouse_lock_y;
public:
	MonitorCutEvent(General &general, int x, int y, int width, int height, KIR4::bitmap &background):
		general(general),
		KIR4::display_event_handle(KIR4_25_CPS, "UYI BitmapMonitorCut", width, height, ALLEGRO_NOFRAME),
		background(background)
	{
		set_position(x, y);
		set_top();
	}

	virtual inline void EVENT_MOUSE_AXES(int X, int Y)
	{
		redrawn();
	}
	virtual inline void EVENT_DRAW(int X, int Y)
	{
		background.draw(0, 0);
		if (mouse_lock)
		{
			al_draw_rectangle(mouse_lock_x, mouse_lock_y, mousex(), mousey(), al_map_rgba(0, 0, 0, 100), 2);
			al_draw_filled_rectangle(mouse_lock_x, mouse_lock_y, mousex(), mousey(), al_map_rgba(100, 100, 100, 100));
		}
	}
	virtual inline void EVENT_TIMER(double cps)
	{
	}
	virtual inline void EVENT_TICK()
	{
	}
	virtual inline void EVENT_MOUSE_BUTTON_DOWN(int BUTTON, int X, int Y)
	{
		if (BUTTON == KIR4::MOUSE_BUTTON_LEFT)
		{
			mouse_lock = true;
			mouse_lock_x = X;
			mouse_lock_y = Y;
		}
		else
			kill();
	}
	virtual inline void EVENT_MOUSE_BUTTON_UP(int BUTTON, int X, int Y)
	{
		if (BUTTON == KIR4::MOUSE_BUTTON_LEFT && mouse_lock)
		{
			int
				max_x,
				max_y,
				min_y,
				min_x;

			if (mouse_lock_x > X)
			{
				max_x = mouse_lock_x;
				min_x = X;
			}
			else
			{
				max_x = X;
				min_x = mouse_lock_x;
			}

			if (mouse_lock_y > Y)
			{
				max_y = mouse_lock_y;
				min_y = Y;
			}
			else
			{
				max_y = Y;
				min_y = mouse_lock_y;
			}

			cutted = background.clone(min_x, min_y, max_x - min_x + 1, max_y - min_y + 1);
			kill();
		}
	}
	virtual inline bool EVENT_KEY_DOWN(int KEY)
	{
		kill(); return false;
	}
	virtual inline bool EVENT_KEY_UP(int KEY)
	{
		kill(); return false;
	}
	virtual inline void EVENT_KEY_CHAR(int KEY)
	{
		kill();
	}
	virtual inline void EVENT_OTHER(int EVENT)
	{
		if (EVENT == ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY)
			kill();
	}

	inline KIR4::bitmap GetCutted()
	{
		return cutted;
	}
};

/*
egy m�r kiv�gott bitmappal t�r vissza
*/
KIR4::bitmap GetMonitorCutBitmap(General &general)
{
	KIR4::bitmap
		bitmap;
	int
		x, y, width = -1, height = -1;

	POINT
		p;
	GetCursorPos(&p);
	ALLEGRO_MONITOR_INFO
		monitor_info;

	for (int i = al_get_num_video_adapters(); i > 0; i--)
	{
		al_get_monitor_info(i - 1, &monitor_info);

		if (monitor_info.x1 <= p.x && monitor_info.y1 <= p.y && monitor_info.x2 > p.x && monitor_info.y2 > p.y)
		{
			x = monitor_info.x1;
			y = monitor_info.y1;
			width = monitor_info.x2 - monitor_info.x1;
			height = monitor_info.y2 - monitor_info.y1;
			bitmap = GetScreen(monitor_info.x1, monitor_info.y1, monitor_info.x2, monitor_info.y2);
			break;
		}
	}

	if (width > 0 && height > 0)
	{
		MonitorCutEvent
			event(general, x, y, width, height, bitmap);

		event.run();

		return event.GetCutted();
	}

	return KIR4::bitmap();
}