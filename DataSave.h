#pragma once

#include "General.h"

std::wstring BitmapSaveName(General *general)
{
	std::wstring
		filename = L"0000000000",
		number = KIR4::str_16::tostr(general->GetID());

	for (unsigned i = 0; i < number.length(); i++)
		filename[i + filename.length() - number.length()] = number[i];

	filename += KIR4::time().wstr(L"%d%m%y_%H%M%S") + std::to_wstring(clock());

	return filename;
}
void BitmapSave(General *general, KIR4::gdi_bitmap bitmap)
{
	if (!bitmap.save((general->GetDir() + L"\\" + BitmapSaveName(general) + L".png").c_str()))
		general->SetError(ReturnSaveError);
}