#pragma once

#include <fstream>

//COMMUNICATE
#define CodeFormatRGB 9
#define CodeFormatRGBA 10
#define CodeFormatPNG 11
#define CodeFormatJPG 12
#define CodeHandshakeRefused 97
#define CodeHandshakeSuccess 98
#define CodeHandshakeRequest 99
#define CodeEndOfData 100
#define CodeFormatAccepted 101
#define CodeFormatRefused 102
#define CodeData 103
#define CodeDataError 104

//RUN
#define FlagUploadData 1
#define FlagSave 2
#define FlagDataFromClipboard 4
#define FlagFullScreenCutData 8
#define FlagMonitorScreenCutData 16
#define FlagDataBrowsingFile 32
#define FlagLinkSetClipboard 64
#define FlagLinkOpen 128
#define FlagDataFromFullScreen 256

//ERRORS
#define ReturnNoError 0
#define ReturnSaveError 1
#define ReturnConnectionError 2
#define ReturnDataSizeError 4

class General
{
	__int32
		flags = 5,//0,
		ID = 0;
	KIR4::basic_string<KIR4::str_16>
		name = "admin66",//L"UNKNOWN",
		serverip = L"localhost",
		serverport = L"4356",
		dir = L"Images";
	bool
		done=false;
	int
		displayw = 640,
		displayh = 480;
	__int32
		error = ReturnNoError;
	float
		status;
	int
		uploadrun = 0;
public:
	General(const General &general):
		flags(general.flags),
		ID(general.ID),
		name(general.name),
		serverip(general.serverip),
		serverport(general.serverport),
		dir(general.dir),
		displayw(general.displayw),
		displayh(general.displayh),
		error(general.error)
	{

	}
	General()
	{

	}
	General(const wchar_t *filename)
	{
		std::wifstream
			file(filename, std::ifstream::binary);

		if (file.is_open())
		{
			file.seekg(0, file.end);
			long size = (long)file.tellg();
			file.seekg(0, file.beg);
			if (size)
			{
				wchar_t
					*data = new wchar_t[size + 1];
				file.read(data, size);
				data[size] = L'\0';
				clog << data << KIR4::eol;
				file.close();

				SetData(KIR4::basic_string<KIR4::str_16>(data));
				delete[] data;
			}
		}
		ApplyUploadPermission();
	}
	~General()
	{
	}

	void ApplyUploadPermission()
	{
		uploadrun = 1;
	}
	void DeclineUploadPermission()
	{
		uploadrun = -1;
	}
	bool WaitForUploadPermission()
	{
		while (uploadrun == 0)
			Sleep(100);
		return uploadrun == 1;
	}
	void Print()
	{
		clog << KIR4::LBLACK << "Version: " << KIR4::WHITE << PROGRAM_VERSION << KIR4::eol;
		clog << KIR4::LBLACK << "Flags: " << KIR4::WHITE << flags << KIR4::eol;
		clog << KIR4::LBLACK << "ID: " << KIR4::WHITE << ID << KIR4::eol;
		clog << KIR4::LBLACK << "Username: " << KIR4::WHITE << name.str() << KIR4::eol;
		clog << KIR4::LBLACK << "Server IP: " << KIR4::WHITE << serverip.str() << KIR4::eol;
		clog << KIR4::LBLACK << "Server port: " << KIR4::WHITE << serverport.str() << KIR4::eol;
		clog << KIR4::LBLACK << "Directory: " << KIR4::WHITE << dir.str() << KIR4::eol;
		clog << KIR4::LBLACK << "Display size: " << KIR4::WHITE << displayw << KIR4::LBLACK << " * " << KIR4::WHITE << displayh << KIR4::eol;
	}
	void SetData(KIR4::basic_string<KIR4::str_16> &args)
	{
		KIR4::basic_string<KIR4::str_16>
			result;
		if ((result = args.get(L"ID'", L"'")).length())
			ID = KIR4::str_16::toi(result);
		if ((result = args.get(L"FLAGS'", L"'")).length())
			flags = KIR4::str_16::toi(result);
		if ((result = args.get(L"NAME'", L"'")).length())
			name = result;
		if ((result = args.get(L"SERVER'", L"'")).length())
		{
			serverip = result.get(L'\0', L':');
			serverport = result.get(L':', L'\0');
		}
		if ((result = args.get(L"DIR'", L"'")).length())
			dir = result;
		if ((result = args.get(L"DISPWH'", L"'")).length())
		{
			displayw = KIR4::str_16::toi(result.get(L'\0', L'*').str());
			displayh = KIR4::str_16::toi(result.get(L'*', L'\0').str());
		}
	}

	//GET
	inline __int32 GetID() const
	{
		return ID;
	}
	inline __int32 GetFlags() const
	{
		return flags;
	}
	inline KIR4::basic_string<KIR4::str_16> GetUsername() const
	{
		return name;
	}
	inline KIR4::basic_string<KIR4::str_16> GetServerIP() const
	{
		return serverip;
	}
	inline KIR4::basic_string<KIR4::str_16> GetServerPort() const
	{
		return serverport;
	}
	inline KIR4::basic_string<KIR4::str_16> GetDir() const
	{
		return dir;
	}
	inline int GetDisplayWidth() const
	{
		return displayw;
	}
	inline int GetDisplayHeight() const
	{
		return displayh;
	}
	inline __int32 GetError()
	{
		return error;
	}
	inline float GetStatus()
	{
		return status;
	}
	inline bool GetDone()
	{
		return done;
	}

	//SET
	inline void SetError(__int32 error)
	{
		this->error |= error;
	}
	inline void SetFlags(__int32 flags)
	{
		this->flags = flags;
	}
	inline void SetID(__int32 ID)
	{
		this->ID = ID;
	}
	inline void SetName(const KIR4::basic_string<KIR4::str_16> name)
	{
		this->name = name;
	}
	inline void SetIP(const KIR4::basic_string<KIR4::str_16> ip)
	{
		this->serverip = ip;
	}
	inline void SetPort(const KIR4::basic_string<KIR4::str_16> port)
	{
		this->serverport = port;
	}
	inline void SetDir(const KIR4::basic_string<KIR4::str_16> dir)
	{
		this->dir = dir;
	}
	inline void SetDisplaySize(int width,int height)
	{
		this->displayw = width;
		this->displayh = height;
	}
	inline void SetStatus(float status)
	{
		this->status = status;
	}
	inline void SetDone(bool done)
	{
		this->done = done;
	}
};