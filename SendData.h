#pragma once

#include <shellapi.h>
#include <list>

#include "General.h"

#include "Format.h"

#define BlockSize 2048//mekkora legyen egy �zenet hossza

void DataBreakUpAndSend(KIR4::plug_socket &socket, const char *data, unsigned long size)
{
	unsigned long
		number = 0;
	unsigned long
		pos = 0;
	KIR4::socket_message
		*message = NULL;

	while (pos + BlockSize <= size)
	{
		number++;
		message = new KIR4::socket_message_dynamic(CodeData);
		message->set_int64(pos);
		message->add_data(&data[pos], BlockSize);
		socket.send_message(message);
		pos += BlockSize;
	}
	if (pos < size)
	{
		number++;
		message = new KIR4::socket_message_dynamic(CodeData);
		message->set_int64(pos);

		message->add_data(&data[pos], size - pos);
		socket.send_message(message);
	}
	clog << "total DATA messages: " << number << KIR4::eol;
}

#define PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE 10
#define PLUG_SOCKET_STATUS_HANDSHAKE_SUCCESS 11
#define PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT_ACCEPT 12
#define PLUG_SOCKET_STATUS_DATA_SEND 13
#define PLUG_SOCKET_STATUS_DATA_SENT 14

void DataUpload(General *general, Format* format)
{
	if (!general->WaitForUploadPermission())
		return;
	KIR4::basic_string<KIR4::str_16>
		directlinks;
	KIR4::clientTCP<KIR4::plug_socket>
		client;
	if (client.connection(KIR4::str_8::tostr(general->GetServerIP().str()).c_str(), KIR4::str_8::tostr(general->GetServerPort().str()).c_str()))
	{
		clog << "ip: " << client.server.get_ip_port() << KIR4::eol;
		KIR4::timer
			connectiontimer,
			sendtimer;
		KIR4::plug_socket::recv_iterator
			mgit = NULL;
		StructOfDataUpload
			*sodu = NULL;
		unsigned long long
			sent_bytes = 0;
		while (client.server.is_running())
		{
		BEGIN:
			clog << "STATUS: " << client.server.get_status() << KIR4::eol;
			if (client.server.get_status() == PLUG_SOCKET_STATUS_DATA_SEND)
			{
				if (sodu && sodu->size)
					clog << "Upload status ( ~ ): " << sent_bytes << " / " << sodu->size << "    " << sent_bytes / double(sodu->size) * 100. << " %" << KIR4::eol;
			}
			else if (client.server.get_status() == PLUG_SOCKET_STATUS_HANDSHAKE_SUCCESS)
			{
				client.server.set_status(PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT_ACCEPT);

			SODU:
				sodu = format->GetNext();

				clog << "SODU OUT: " << sodu << KIR4::eol;
				if (!sodu)
				{
					clog.color(KIR4::LRED) << "UNSUCC!" << KIR4::eol;
					client.server.kill();
					goto END;
				}
				else
				{
					bool
						test = sodu->Test();
					clog << "Test: " << test << KIR4::eol;
					if (sodu->Test())
					{
						clog.color(KIR4::LRED) << "SUCC!" << KIR4::eol;
					}
					else
					{
						clog.color(KIR4::LRED) << "SODURES!" << KIR4::eol;
						goto SODU;
					}
				}


				client.server.send_message(sodu->format);
				sodu->format = NULL;

				clog << "[SEND]: " << "CodeFormat*" << KIR4::eol;
			}
			else if (client.server.get_status() == PLUG_SOCKET_STATUS_RUNNING)
			{
				client.server.set_status(PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE);
				KIR4::socket_message
					*handshake = new KIR4::socket_message_dynamic(CodeHandshakeRequest);
				handshake->set_wstr(general->GetUsername());
				handshake->set_wstr(PROGRAM_VERSION);
				client.server.send_message(handshake);
				clog << "[SEND] " << "CodeHandshakeRequest" << KIR4::eol;
			}

			mgit = client.server.receive_begin();
			if (mgit != client.server.receive_end())
			{
				while (mgit != client.server.receive_end())
				{
					if ((**mgit).get_code() != KIR4_SOCKET_CODE_USED)
					{
						switch ((*mgit)->get_code())
						{
							case CodeHandshakeSuccess:
							{
								clog << "[RECV]: " << "CodeHandshakeSuccess" << KIR4::eol;
								if (PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE)
								{
									client.server.set_status(PLUG_SOCKET_STATUS_HANDSHAKE_SUCCESS);
									clog << "Server version: " << (**mgit).get_swstr() << KIR4::eol;
									(**mgit).proccessed();
									goto BEGIN;
								}
								(**mgit).proccessed();
								break;
							}
							case CodeFormatAccepted:
							{
								clog << "[RECV]: " << "CodeFormatAccepted" << KIR4::eol;
								if (client.server.get_status() == PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT_ACCEPT)
								{
									sent_bytes = 0;
									client.server.set_status(PLUG_SOCKET_STATUS_DATA_SEND);
									DataBreakUpAndSend(client.server, sodu->data, sodu->size);
									sodu->Destroy();
									sendtimer.restart();
									clog << "[SEND] " << "Data" << KIR4::eol;
								}
								(**mgit).proccessed();
								break;
							}
							case CodeFormatRefused:
							{
								clog << "[RECV]: " << "CodeFormatRefused: ";

								auto
									reason = (**mgit).get_swstr();
								if (reason)
									clog << reason << KIR4::eol;
								else
									clog << "No reason" << KIR4::eol;

								client.server.kill();
								break;
							}
							case CodeHandshakeRefused:
							{
								clog << "[RECV]: " << "CodeHandshakeRefused" << KIR4::eol;

								auto
									reason = (**mgit).get_swstr();
								if (reason)
								{
									clog << reason << KIR4::eol;
								}
								else
									clog << "No reason" << KIR4::eol;

								client.server.kill();
								break;
							}
							case CodeEndOfData:
							{
								clog << "[RECV] " << "CodeEndOfData" << KIR4::eol;
								if (client.server.get_status() == PLUG_SOCKET_STATUS_DATA_SEND)
								{
									client.server.set_status(PLUG_SOCKET_STATUS_DATA_SENT);
									double
										elapsed = sendtimer;
									sent_bytes += client.server.get_send_bytes();
									clog << "- - - - - - -" << KIR4::eol;
									clog << "Elapsed time: " << elapsed << KIR4::eol;
									clog << "Sent bytes: " << sent_bytes << "  ( MB: " << sent_bytes / double(1048576) << " )" << KIR4::eol;
									clog << "Upload speed: " << ((sent_bytes / double(1048576)) * 8) / elapsed << "Mb/s" << KIR4::eol;
									auto
										link = (*mgit)->get_swstr();
									if (link)
									{
										clog << "received link: " << link << KIR4::eol;
										if (general->GetFlags()&FlagLinkOpen)
											ShellExecuteW(NULL, L"open", link, NULL, NULL, SW_SHOWNORMAL);
										if (general->GetFlags()&FlagLinkSetClipboard)
										{
											if (directlinks.size())
												directlinks += L"\n";
											directlinks += link;
										}

									}
									client.server.set_status(PLUG_SOCKET_STATUS_HANDSHAKE_SUCCESS);
									(**mgit).proccessed();
									goto BEGIN;
								}
								(**mgit).proccessed();
								break;
							}
							default:
							{
								unsigned
									code = (*mgit)->get_code();
								clog << "[RECV] " << code << " - MESSAGE DROP" << KIR4::eol;
							}
						}
					}
					mgit++;
				}
				client.server.recv_message_collector();
			}

			Sleep(300);
			sent_bytes += client.server.get_send_bytes();
		END:
			;
		}
		client.disconnection();
	}
	else
		general->SetError(ReturnConnectionError);
	if (general->GetFlags()&FlagLinkSetClipboard)
	{
		clog << "LINKS:\n";
		clog << directlinks.str() << KIR4::eol;
		KIR4::set_clipboard_w(directlinks);
	}
}