#pragma once

#include "General.h"

struct StructOfDataUpload
{
	char
		*data = NULL;
	unsigned long
		size = 0;
	KIR4::socket_message
		*format = NULL;
	int
		flags = 0;
	StructOfDataUpload()
	{

	}
	~StructOfDataUpload()
	{
		Destroy();
	}
	void Destroy()
	{
		if (data)
		{
			delete[] data;
			data = NULL;
		}
		if (format)
		{
			delete format;
			format = NULL;
		}
	}
	inline bool Test()
	{
		clog << "Data: " << (void*)data << KIR4::eol;
		clog << "Size: " << size << KIR4::eol;
		clog << "Format: " << (void*)format << KIR4::eol;
		return data != NULL && size > 0 && format != NULL;
	}
};

class Format
{
protected:
	General
		*general;
public:
	Format(General *general):general(general)
	{

	}
	virtual StructOfDataUpload *GetNext() = 0;
};