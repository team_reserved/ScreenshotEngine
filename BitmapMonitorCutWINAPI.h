#pragma once

#include "GetFullScreen.h"
#include "General.h"

#define MonitorCutEventIDTtimer 1
class MonitorCutEvent
{
	General
		*general;
	KIR4::gdi_bitmap
		&background,
		cutted;
	bool
		mouse_lock = false;
	int
		mouse_lock_x,
		mouse_lock_y,
		width,
		height,
		mousex,
		mousey,
		min_x,
		min_y,
		max_x,
		max_y;
	void SetMinMax()
	{
		if (mouse_lock_x > mousex)
		{
			max_x = mouse_lock_x;
			min_x = mousex;
		}
		else
		{
			max_x = mousex;
			min_x = mouse_lock_x;
		}

		if (mouse_lock_y > mousey)
		{
			max_y = mouse_lock_y;
			min_y = mousey;
		}
		else
		{
			max_y = mousey;
			min_y = mouse_lock_y;
		}
	}


	HWND
		hWnd;
	int
		redrawn = 3;
public:
	MonitorCutEvent(General *general, KIR4::gdi_bitmap &background):
		general(general),
		background(background)
	{
	}
	inline void Redrawn()
	{
		redrawn |= 1;
	}
	static LRESULT CALLBACK Event(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		HDC          hdc;
		PAINTSTRUCT  ps;
		MonitorCutEvent
			*c = (MonitorCutEvent*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

		switch (message)
		{
			case WM_MOUSEMOVE:
			{
				if (c->mouse_lock)
				{
					c->mousex = LOWORD(lParam);
					c->mousey = HIWORD(lParam);
					c->Redrawn();
				}
				return 0;
			}
			case WM_TIMER:
			{
				if (wParam == MonitorCutEventIDTtimer)
				{
					c->redrawn |= 2;
					if (c->redrawn == 3)
						InvalidateRect(hWnd, NULL, FALSE);
				}
				return 0;
			}
			case WM_PAINT:
			{
				HDC
					hdcMem;
				HBITMAP
					hbmMem;
				HANDLE
					hOld;

				hdc = BeginPaint(hWnd, &ps);

				if (c->redrawn == 3)
				{
					c->redrawn = 0;

					hdcMem = CreateCompatibleDC(hdc);
					hbmMem = CreateCompatibleBitmap(hdc, c->width, c->height);

					hOld = SelectObject(hdcMem, hbmMem);

					c->SetMinMax();
					c->EVENT_DRAW(hdcMem);

					BitBlt(hdc, 0, 0, c->width, c->height, hdcMem, 0, 0, SRCCOPY);

					SelectObject(hdcMem, hOld);
					DeleteObject(hbmMem);
					DeleteDC(hdcMem);

				}
				EndPaint(hWnd, &ps);
				return 0;
			}
			case WM_LBUTTONDOWN:
			{
				c->mousex = LOWORD(lParam);
				c->mousey = HIWORD(lParam);
				c->mouse_lock_x = c->mousex;
				c->mouse_lock_y = c->mousey;
				c->mouse_lock = true;
				c->Redrawn();
				return 0;
			}
			case WM_LBUTTONUP:
			{
				if (c->mouse_lock)
				{
					c->mouse_lock = false;
					c->cutted = c->background.clone(int(c->min_x / double(c->width)*c->background.width()), int(c->min_y / double(c->height)*c->background.height()), int((c->max_x - c->min_x) / double(c->width)*c->background.width() + 1), int((c->max_y - c->min_y) / double(c->height) * c->background.height() + 1));
					DestroyWindow(hWnd);
				}
				return 0;
			}
			case WM_RBUTTONUP:
			{
				c->mouse_lock = false;
				c->Redrawn();
				return 0;
			}
			case WM_KILLFOCUS:
			{
				DestroyWindow(hWnd);
				return 0;
			}
			case WA_INACTIVE:
			{
				DestroyWindow(hWnd);
				return 0;
			}
			case WM_MOUSELEAVE:
			{
				DestroyWindow(hWnd);
				return 0;
			}
			case WM_KEYDOWN:
			{
				DestroyWindow(hWnd);
				return 0;
			}
			case WM_ERASEBKGND:
			{
				return 1;
			}
			case WM_DESTROY:
			{
				KillTimer(hWnd, MonitorCutEventIDTtimer);
				PostQuitMessage(0);
				return 0;
			}
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	void run(int x, int y, int w, int h)
	{
		MSG
			msg;
		WNDCLASS
			wndClass;

		wndClass.style = CS_HREDRAW | CS_VREDRAW;
		wndClass.lpfnWndProc = Event;
		wndClass.cbClsExtra = 0;
		wndClass.cbWndExtra = 0;
		wndClass.hInstance = 0;
		wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wndClass.hCursor = LoadCursor(NULL, IDC_CROSS);
		wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		wndClass.lpszMenuName = NULL;
		wndClass.lpszClassName = TEXT("STATIC");

		RegisterClass(&wndClass);

		hWnd = CreateWindowEx(WS_EX_TOPMOST, "STATIC", NULL, WS_POPUP, x, y, w, h, NULL, NULL, NULL, NULL);

		SetTimer(hWnd, MonitorCutEventIDTtimer, 1000 / 24, (TIMERPROC)NULL);

		width = w;
		height = h;
		clog << "WINDOW: " << w << " * " << h << KIR4::eol;

		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)this);
		SetCursor(LoadCursor(NULL, IDC_CROSS));

		ShowWindow(hWnd, SW_SHOW);
		UpdateWindow(hWnd);

		while (GetMessage(&msg, hWnd, 0, 0) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	virtual inline void EVENT_DRAW(HDC hdc)
	{
		Gdiplus::Graphics
			graphics(hdc);
		background.draw_scaled(float(0), float(0), float(width), float(height), graphics);
		if (mouse_lock)
		{
			Gdiplus::Pen
				pen(Gdiplus::Color(120, 20, 20, 20), 2);
			Gdiplus::SolidBrush
				solidBrush(Gdiplus::Color(120, 120, 120, 120));

			Gdiplus::Rect
				rec;
			rec.X = min_x - 2;
			rec.Y = min_y - 2;
			rec.Width = max_x - min_x + 4;
			rec.Height = max_y - min_y + 4;

			graphics.DrawRectangle(&pen, rec);
			graphics.FillRectangle(&solidBrush, min_x, min_y, max_x - min_x, max_y - min_y);
		}
	}
	inline KIR4::gdi_bitmap GetCutted()
	{
		return cutted;
	}
};

struct MonitorInfo
{
	POINT
		cursor;
	int
		top,
		bottom,
		left,
		right;
};

BOOL CALLBACK GetMonitorInfo(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcWork, LPARAM dwData)
{
	MonitorInfo
		*info = (MonitorInfo*)dwData;

	MONITORINFO
		monitor_info;
	monitor_info.cbSize = sizeof(MONITORINFO);
	if (GetMonitorInfoW(hMonitor, &monitor_info))
	{
		if (monitor_info.rcMonitor.left <= info->cursor.x && monitor_info.rcMonitor.top <= info->cursor.y && monitor_info.rcMonitor.right > info->cursor.x && monitor_info.rcMonitor.bottom > info->cursor.y)
		{
			info->left = monitor_info.rcMonitor.left;
			info->top = monitor_info.rcMonitor.top;
			info->bottom = monitor_info.rcMonitor.bottom;
			info->right = monitor_info.rcMonitor.right;
		}
	}

	return true;
}


KIR4::gdi_bitmap GetMonitorCutBitmap(General *general)
{
	MonitorInfo
		info;
	GetCursorPos(&info.cursor);

	EnumDisplayMonitors(NULL, NULL, GetMonitorInfo, (LPARAM)&info);

	KIR4::gdi_bitmap
		bitmap = GetScreen(info.left, info.top, info.right, info.bottom);

	MonitorCutEvent
		ev(general, bitmap);

	ev.run(info.left, info.top, info.right - info.left, info.bottom - info.top);

	return ev.GetCutted();
}