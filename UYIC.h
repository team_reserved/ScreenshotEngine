#pragma once

#define WIN32_LEAN_AND_MEAN

//#include <KIR\KIR4_debug.h>
//#define KIR4_SOCKET_LOG

const wchar_t PROGRAM_VERSION[] = L"2.0.0.3A - EZ";

#include <KIR\KIR4_console.h>
#include <KIR\KIR4_system.h>
#include <KIR\KIR4_basic_string.h>
#include <KIR\KIR4_GDI_bitmap.h>
//#include <KIR\KIR4_utf8.h>
#include <KIR\KIR4_clientA.h>
//#include <KIR\KIR3_bitmap_operation.h>

#include "General.h"
#include "SendData.h"
#include "DataSave.h"
#include "BitmapFullScreenCutWINAPI.h"
#include "BitmapMonitorCutWINAPI.h"
//#include "BitmapMonitorCut.h"
//#include "BitmapUpload.h"
//#include "FileUpload.h"
//#include "Testing.h"

#include "FormatFile.h"
#include "FormatPNG.h"
#include "FormatRGB.h"
#include "FormatRGBA.h"

void UYIC(General *general)
{
	general->Print();

	if (general->GetFlags()&FlagDataFromFullScreen)
	{
		clog << "FlagDataFromFullScreen" << KIR4::eol;
		FormatPNG
			data(general, GetFullScreen());
		if (general->GetFlags()&FlagUploadData)
		{
			clog << "FlagUploadData" << KIR4::eol;
			DataUpload(general, &data);
		}
		if (general->GetFlags()&FlagSave)
		{
			clog << "FlagSave" << KIR4::eol;
			data.Save();
		}
	}
	else if (general->GetFlags()&FlagFullScreenCutData)
	{
		clog << "FlagFullScreenCutData" << KIR4::eol;
		FormatPNG
			data(general, GetFullScreenCutBitmap(general));
		if (general->GetFlags()&FlagUploadData)
		{
			clog << "FlagUploadData" << KIR4::eol;
			DataUpload(general, &data);
		}
		if (general->GetFlags()&FlagSave)
		{
			clog << "FlagSave" << KIR4::eol;
			data.Save();
		}
	}
	else if (general->GetFlags()&FlagMonitorScreenCutData)
	{
		clog << "FlagMonitorScreenCutData" << KIR4::eol;
		FormatPNG
			data(general, GetMonitorCutBitmap(general));
		if (general->GetFlags()&FlagUploadData)
		{
			clog << "FlagUploadData" << KIR4::eol;
			DataUpload(general, &data);
		}
		if (general->GetFlags()&FlagSave)
		{
			clog << "FlagSave" << KIR4::eol;
			data.Save();
		}
	}
	else if (general->GetFlags()&FlagDataFromClipboard)
	{
		clog << "FlagDataFromClipboard" << KIR4::eol;
		HBITMAP
			hbitmap = KIR4::get_clipboard_hbitmap();
		HPALETTE
			hpalette = 0;
		KIR4::gdi_bitmap
			bitmap = new Gdiplus::Bitmap(hbitmap, hpalette);
		clog << hbitmap << KIR4::eol;
		clog << bitmap << KIR4::eol;
		FormatPNG
			data(general, bitmap);
		if (general->GetFlags()&FlagUploadData)
		{
			clog << "FlagUploadData" << KIR4::eol;
			DataUpload(general, &data);
		}
		if (general->GetFlags()&FlagSave)
		{
			clog << "FlagSave" << KIR4::eol;
			BitmapSave(general, bitmap);
		}
	}
	else if (general->GetFlags()&FlagDataBrowsingFile)
	{
		clog << "FlagDataBrowsingFile" << KIR4::eol;
		FormatFile
			data(general, KIR4::browse_for_files_w(L"Pictures\0*.PNG;*.JPG;*.BMP"));
		if (general->GetFlags()&FlagUploadData)
		{
			clog << "FlagUploadData" << KIR4::eol;
			DataUpload(general, &data);
		}
		if (general->GetFlags()&FlagSave)
		{
			clog << "FlagSave" << KIR4::eol;
			data.Save();

		}
	}
	clog << "Return: " << general->GetError() << KIR4::eol;

	general->SetDone(true);
}